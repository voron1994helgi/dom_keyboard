Теоретичні питання
1. Як можна визначити, яку саме клавішу клавіатури натиснув користувач?
Можна використати подію keydown та прописати перевірку на нажату клавішу, наприклад : 

document.addEventListener('keydown', function(event) {
  if (event.code === 'KeyA') {
    console.log('Користувач натиснув клавішу "A"');
  }
});

2. Яка різниця між event.code() та event.key()?

event.code() показує чи була нажата саме клавіша , не залежно від нажатих інших клавіш типу Shift , Ctrl , Alt і  т.п.; евент.код представляє саме фізичну клавішу клавіатури ,нзалежно навіть від розкладки. Натискання клавіші "A" завжди буде генерувати event.code === "KeyA"
event.key() представляє із себе саме символ або  комбінацію клавіш .Наприклад, натискання клавіші "A" може генерувати event.key === "A" або event.key === "Shift+A", залежно від того, чи натиснута клавіша Shift.

3. Які три події клавіатури існує та яка між ними відмінність?

keydown - виконується , коли кнопка нажата
keyup- виконується , коли кнопка відпущена
keypress- виконується , коли кнопка зажата

Відмінності :

Час виникнення: keydown виникає першим, потім keypress (якщо генерується символ), і потім keyup.
Тип інформації: keydown і keyup не несуть інформації про сам символ, лише про те, чи натиснута клавіша або відпущена.keypress надає інформацію про натиснутий символ.
Застосування: keydown часто використовується для комбінацій клавіш і запобігання стандартній поведінці,keyup використовується разом з keydown для більш точного відстеження, а keypress використовується для введення тексту.