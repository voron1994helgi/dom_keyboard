/* 

Практичне завдання.
Реалізувати функцію підсвічування клавіш.

Технічні вимоги:

- У файлі index.html лежить розмітка для кнопок.
- Кожна кнопка містить назву клавіші на клавіатурі
- Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.
*/




document.addEventListener("keydown", (event) => {
    const buttons = document.querySelectorAll(".btn");
    buttons.forEach(button => {
        const buttonText = button.textContent;
        if (event.key === buttonText.toLowerCase()) {
            button.classList.add('active');
        } else {
            button.classList.remove('active');
        }
        if (event.key === buttonText) {
            button.classList.add('active');
        } else if (event.key === buttonText) {
            button.classList.add('active');
        }
    })
})


